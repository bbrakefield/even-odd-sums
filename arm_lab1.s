.text
.global _start

_start:
        @set up stack
        LDR sp, =stack
        ADD sp, sp, #100      	@ since we use push and pop, it assumes a
                              	@ full descending (FD) stack
                              	@ so stack pointer should start at the higher
                              	@ addresses and the stack will grow up.
        MOV R0, #1              @ counter
        MOV R5, #0              @ odd sum
        MOV R6, #0              @ even sum
        MOV R2, #0              @ result

whileLoop:
        CMP R0, #26
        BEQ endLoop
        AND R2, R0, #1

oddRoutine:
        CMP R2, #1
        BNE endOdd

        @ This is the value I want to print
        MOV   R1, R0
        BL outDec             	@ call subroutine to print the value
        BL newline            	@ print a newline
        ADD R5, R5, R0

endOdd:
        ADD R0, R0, #1
        B whileLoop

endLoop:
        MOV R1, R5
        BL newline
        BL outDec
        BL newline
        BL newline
        MOV R0, #1		@ reinitialize counter
        MOV R2, #0		@ reinitialized result

whileLoop2:
      	CMP R0, #26
      	BEQ endLoop2
      	AND R2, R0, #1

evenRoutine:
      	CMP R2, #0
      	BNE endEven

        @ This is the value I want to print
        MOV R1, R0
        BL outDec
        BL newline
        ADD R6, R6, R0

endEven:
      	ADD R0, R0, #1
      	B whileLoop2

endLoop2:
        MOV R1, R6
        BL newline
        BL outDec
        BL newline
        BL newline
      	MOV R0, #0
      	ADD R0, R5, R6

        @ print the total sum here
        MOV R1, R0
        BL outDec
        BL newline

        @ terminate the program - must do this for the program to
        @ run correctly
        MOV R7, #0x01		@ doing an exit
        SVC 0			@ do the system call

print:
        MOV R7, #1
        SVC 0

outDec:
        @ This routine expects the number to be printed to be in r1
        PUSH  {R0-R4, R6, R8, lr}     @ save working registers & link register
        MOV   R8, #0
        MOV   R4, #0                  @ number of digits in number to print

outNext:
        MOV   R8, R8, LSL #4
        ADD   R4, R4, #1
        BL    div10                   @ quotient will be in r1 and remainder in r2
        ADD   R8, R8, R2              @ insert remainder (least significant digit)
        CMP   R1, #0                  @ if quotient zero then all done
        BNE   outNext                 @ else deal with the next digit

outNxt1:
        AND   R0, R8, #0xF
        ADD   R0, R0, #0x30
        LDR   R6, =value
        STR   R0, [R6]                @ copy value in r0 to our storage area (value)
        MOVS  R8, R8, LSR #4
        BL    putCh
        SUBS  R4, R4, #1              @ decrement counter
        BNE   outNxt1                 @ repeat until all printed

outEx:
        POP {R0-R4, R6, R8, pc}       @ restore registers and return (end of outDec)

div10:                                @ divide r1 by 10
                                      @ return with quotient in r1, remainder in r2
        SUB   R2, R1, #10
        SUB   R1, R1, R1, LSR #2
        ADD   R1, R1, R1, LSR #4
        ADD   R1, R1, R1, LSR #8
        ADD   R1, R1, R1, LSR #16
        MOV   R1, R1, LSR #3
        ADD   R3, R1, R1, ASL #2
        SUBS  R2, R2, R3, ASL #1
        ADDPL R1, R1, #1
        ADDMI R2, R2, #10
        MOV   pc, lr                  @ exit div10

putCh:
        PUSH {R0-R2, R7, lr}          @ save working registers

        @ write the value
        MOV   R7, #4                  @ for printing
        MOV   R0, #1                  @ for standard output
        MOV   R2, #1                  @ buffer size
        LDR   R1, =value              @ address of value
        SVC   0                       @ invoke kernel

        POP {R0-R2, R7, pc}           @ exit putCh

newline:
        PUSH {R0-R2, R7, lr}	      @ save working registers

        @ write a newline
        MOV  R7, #0x04		      @ doing a write
        MOV  R0, #0x01		      @ file descriptor = standard output
        MOV  R2, #1           	      @ buffer size (no. bytes to write)
        LDR  R1, =nl          	      @ put address of message in R1
        SVC  0                	      @ do the system call

        POP {R0-R2, R7, pc}           @ exit newline

@-------------------------
@ data area of our program

.data

value:  .word 0
stack:  .space 0x100, 0	@ set up stack
nl:     .ascii "\n"
